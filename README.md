#### 目标

   - 想做一个 Stata 资料速查表，包括：FAQ，常用命令，Blogs。

   - 目前只在 [wiki](https://gitee.com/arlionn/stata/wikis/Home) 中搭建了简单的框架，还需更多的同仁们一起参与来完成这个项目。

#### 基本主题(待补充)

   - 数据处理
   - 绘图
   - Stata程序
   - 面板数据模型(Panel data models)
   - IV-GMM估计
   - 倍分法 (DID)
   - 倾向得分匹配分析 (PSM)
   - 断点回归分析 (RDD)
   - 合成控制法 (SCM)